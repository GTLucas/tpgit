#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 15 17:32:25 2023

@author: lucasonelife
"""

import os
import sys

# give a fasta file as argument


def empty_file(fastafile):
    """check if it's an empty file"""
    if os.path.getsize(fastafile) == 0:
        result = fastafile + " : " + "This is an empty file" + "\n"
        with open('errorfile', 'a') as error:
            error.write(result)
        sys.exit()
    print('This is a valid file')

# give a fasta file as argument


def fasta_format(fastafile):
    """check file extension"""
    full_path_name = fastafile.split(".")
    extension = full_path_name[1]
    if extension != "fasta":
        result = fastafile + " : " + \
            "This is not a fasta format, check your file please !" + "\n"
        with open('errorfile', 'a') as error:
            error.write(result)
        sys.exit()
    print('This is a fasta extension')

# give a fasta file as argument


def fasta(fastafile):
    """check file header"""
    with open(fastafile, "r") as file:
        for base in file:
            header = base.count(">")
            if header == 0:
                result = fastafile + " : " + \
                    "This is not a fasta file, check your file please !" + "\n"
                with open('errorfile', 'a') as error:
                    error.write(result)
                sys.exit()
            print("This is a fasta file")
            return True

# give a fasta file as argument


def adn_read(fastafile):
    """check if the sequences in fasta file are nucleic acids"""
    dna_list = ["A", "C", "G", "T"]
    with open(fastafile, "r") as file:
        sequences = file.readlines()
        print(sequences)
        counter = 0
        header = ""
        for line in sequences:
            counter += 1
            if line[0] == ">":
                header = line.strip()
            else:
                line = line.strip()
                line = line.upper()
                column_counter = 0
                for char in line:
                    column_counter += 1
                    if char not in dna_list:
                        result = fastafile + " : " + char + \
                            " It's not a nucl in line " + \
                            str(counter) + " and column " + str(column_counter) + \
                            " for sequence " + header[1:] + "\n"
                        with open('errorfile', 'a') as error:
                            error.write(result)


print(sys.argv)
for arg in sys.argv[1:]:
    if os.path.exists(arg) is False:
        print("The file " + str(arg) + " does not exist")
    else:
        empty_file(arg)
        fasta_format(arg)
        fasta(arg)
        adn_read(arg)
